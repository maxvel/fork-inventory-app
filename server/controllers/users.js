import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
import  Mongoose  from 'mongoose';
import Product from '../models/product.js';
import User from '../models/user.js'

export const signin= async(req, res)=>{
    const {email, password} = req.body;

    try {
        const existingUser = await User.findOne({ email });
        if(!existingUser) return res.status(404).json({message:"user doesn't exist."});

        const isPasswordCorrect = await bcrypt.compare(password, existingUser.password);
        if(!isPasswordCorrect) return res.status(400).json({message:"Invalid credentials."});

        const token = jwt.sign({ email: existingUser.email, id:existingUser._id}, 'test', {expiresIn: "1h"});

        res.status(200).json({ result: existingUser, token});
    } catch (error) {
        res.status(500).json({ message: 'Something went wrong'});
    }
}

export const signup = async (req, res)=> {
    const { email, password, confirmPassword, firstName, lastName} = req.body;
    
    try {
        const existingUser = await User.findOne({ email });
        if(existingUser) return res.status(400).json({message:"user already exist."});
    
         if(password != confirmPassword) return res.status(400).json({message:"Password Incorrect"});

        const hashedpassword = await bcrypt.hash(password, 12);
        const result = await User.create({email, password: hashedpassword, name:`${firstName} ${lastName}`});
        const token = jwt.sign({ email: result.email, id:result._id}, 'test', {expiresIn: "1h"});

        res.status(200).json({ result , token });

    } catch (error) {
        res.status(500).json({ message: 'Something went wrong'}); 
    }
}

export const updateProfile = async (req, res) => {
    const {id:_id} = req.params;
    const {firstName, lastName, target, email} = req.body;

    if (!Mongoose.Types.ObjectId.isValid(_id)) return res.status(404).send(`No user with id: ${id}`);

    const updatedUser = await User.findByIdAndUpdate(_id, { name:`${firstName} ${lastName}`, email:email, target:target, _id},{ new:true });

    res.json(updatedUser);
}

export const changePassword = async (req, res) => {
    const {id:_id} = req.params;

    let { oldPassword, newPassword } = req.body;
   
    if (!oldPassword || !newPassword) {
      return res.json({ message: "All filled must be required" });
    } else {
      const data = await User.findOne({ _id });
      if (!data) {
        return res.json({
          error: "Invalid user",
        });
      } else {
        const oldPassCheck = await bcrypt.compare(oldPassword, data.password);
        if (oldPassCheck) {
          newPassword = bcrypt.hash(newPassword, 12);
          let passChange = User.findByIdAndUpdate(_id, {
            password: newPassword,
          });
          passChange.exec((err, result) => {
            if (err) console.log(err);
            return res.json({ success: "Password updated successfully" });
          });
        } else {
          return res.json({
            error: "Your old password is wrong!!",
          });
        }
      }
    }
  

}