import mongoose from 'mongoose';
import Categories from '../models/categories.js';

export const getCategories = async (req, res) => { 
    try {
        const categories = await Categories.find().populate('category');
                
        res.status(200).json(categories);
    } catch (error) {
        res.status(404).json({ message: error.message });
    }
}

export const createCategories = async (req, res) => {
    const category = req.body;

    const newCategories = new Categories({ ...category })

    try {
        await newCategories.save();

        res.status(201).json(newCategories );
    } catch (error) {
        res.status(409).json({ message: error.message });
    }
}

export const updateCategories = async (req, res) => {
    const { id: _id } = req.params;
    const category = req.body;
    
    if (!mongoose.Types.ObjectId.isValid(_id)) return res.status(404).send(`No category with id: ${id}`);

    const updatedCategory =  await Categories.findByIdAndUpdate(_id, {...category, _id}, { new: true });

    res.json(updatedCategory);
}

export const deleteCategory = async (req, res) => {
    const { id } = req.params;

    if (!mongoose.Types.ObjectId.isValid(id)) return res.status(404).send(`No category with id: ${id}`);

    await Categories.findByIdAndRemove(id);

    res.json({ message: " deleted successfully." });
}