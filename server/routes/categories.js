import express from 'express';

import {getCategories, deleteCategory, updateCategories, createCategories } from '../controllers/categories.js';

const router = express.Router();
import auth from "../middleware/auth.js";

router.get('/', getCategories);
router.post('/', createCategories);
router.patch('/:id', updateCategories);
router.delete('/:id', deleteCategory);

export default router;