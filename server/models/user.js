import mongoose from 'mongoose';

const UserSchema = mongoose.Schema({
    name: {  
        type: String,
        required: true
     },
    email: {
        type: String,
        required: true
     },
    password: { 
        type: String,
        required: true
     },
     product:{
        type: [String], 
        ref: 'Product',
        default:[]
     },
     transaction:{
        type: [String], 
        ref: 'transaction',
        default:[]
     },
     total_profits:{
        type: Number,
        default: 0
     },
     total_revenue:{
        type:Number,
        default: 0
     },
     total_orders:{
         type:Number,
         default: 0
     },
     target:{
        type: Number,
        default: 0
     }
});

const User = mongoose.model('User',UserSchema);

export default User;