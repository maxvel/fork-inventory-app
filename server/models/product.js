import mongoose from 'mongoose';

const ProductSchema = mongoose.Schema({
    item_name:{
        type: String,
        required: true
    },
    brand:{
        type: String
    },
    category:{
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'Category'
    },
    stock:{
        type: Number,
        required: true
    },
    buy_price:{
        type: Number,
        required: true
    },
    sell_price:{
        type:Number,
        require: true
    },
    created_by:{
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'User'
    },
});

const Product = mongoose.model('Product',ProductSchema);

export default Product;