import React from 'react';
import HomeIcon from '@material-ui/icons/Home';
import AddIcon from '@material-ui/icons/Add';
import ReceiptIcon from '@material-ui/icons/Receipt';
import CategoryIcon from '@material-ui/icons/Category';
import { makeStyles } from "@material-ui/core";
import IconButton from '@material-ui/core/IconButton';
import {Link} from 'react-router-dom';
import AirplayOutlinedIcon from '@material-ui/icons/AirplayOutlined';
import Tooltip from '@material-ui/core/Tooltip';

const useStyles = makeStyles(theme => ({
    sideMenu: {
        display: 'flex',
        flexDirection: 'column',
        position: 'absolute',
        left: '0px',
        width: '80px',
        height: '100%',
        backgroundColor: '#10100f',
    },
    sideIcon:{
        display:"flex",
        alignItems:'center',
        justifyContent:'center',
        margin:20
    },
    icon:{
        display:"flex",
        alignItems:'center',
        justifyContent:'center',
        flexDirection:'column',

    },
    Tooltip:{
        color: theme.palette.common.black,
    }
}))

 

function Sidebar() {
    const classes = useStyles();
    return (
        <div className={classes.sideMenu}>
            <div className={classes.sideIcon}>
                <AirplayOutlinedIcon  fontSize="large" color="primary"  />
            </div>
            <div className={classes.icon}>
                <IconButton aria-label="" >
                    <Tooltip title="Home" arrow className={classes.Tooltip}>
                    <Link to='/'>
                        <HomeIcon fontSize="large" color="primary"/>
                    </Link>
                    </Tooltip>
                </IconButton>
                <IconButton aria-label="" >
                    <Tooltip title="Add Product" arrow className={classes.Tooltip}>
                        <Link to='/addItems'>
                            <AddIcon fontSize="large" color="primary"/>
                        </Link>
                    </Tooltip>
                </IconButton>
                <IconButton aria-label="" >
                    <Tooltip title="Add Transaction" arrow className={classes.Tooltip}>
                        <Link to='/addTransaction'>
                            <ReceiptIcon fontSize="large" color="primary"/> 
                        </Link>
                    </Tooltip>    
                </IconButton>
                <IconButton aria-label="Add Category" >
                    <Tooltip title="Add Category" arrow className={classes.Tooltip}>
                        <Link to='/AddCategory'>
                            <CategoryIcon fontSize="large" color="primary"/> 
                        </Link>
                    </Tooltip>
                </IconButton>
            </div>

        </div>
    )
}

export default Sidebar
