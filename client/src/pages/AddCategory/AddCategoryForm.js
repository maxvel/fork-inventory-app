import React , {useEffect} from 'react'
import { Grid, } from '@material-ui/core';
import Controls from "../../components/controls/Controls";
import { useForm, Form } from '../../components/useForm';


const user = JSON.parse(localStorage.getItem('profile'));
// console.log(user.result._id);

const initialValues ={
    item_name:'',
    category_description:'',
    tax:'',
    created_by: user? user.result._id : '613f8fcec29ba8368002d0e7' 
}



function AddCategoryForm(props) {

    const { addOrEdit, recordForEdit } = props


    const handleSubmit = e => {
        e.preventDefault();
        addOrEdit(values, resetForm)
    }

    const {
        values,
        setValues,
        handleInputChange,
        resetForm
    } = useForm(initialValues);

    useEffect(()=> {
        if(recordForEdit != null)
        setValues({...recordForEdit})
    },[recordForEdit])


    return (
        <Form onSubmit={handleSubmit}>
             <Grid container>
                <Grid item xs={12}>
                    <Controls.Input
                        name="item_name"
                        label="Category Name"
                        value={values.item_name}
                        onChange={handleInputChange}
                    />
                    <Controls.Input
                        label="Category Description"
                        name="category_description"
                        value={values.category_description}
                        onChange={handleInputChange}
                        multiline
                    />
                   <Controls.Input
                        label="Tax (%)"
                        name="tax"
                        value={values.tax}
                        onChange={handleInputChange}
                        multiline
                    />

                    <div>
                        <Controls.Button
                            type="submit"
                            text="Submit" />
                        <Controls.Button
                            text="Reset"
                            color="default"
                            onClick={resetForm} />
                    </div>
              </Grid>

             </Grid>
        </Form>
    )
}

export default AddCategoryForm
