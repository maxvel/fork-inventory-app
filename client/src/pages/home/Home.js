import React, {useState, useEffect} from 'react';
import Infobox from '../../components/Infobox';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, Typography, Button } from '@material-ui/core';
import ShowChartIcon from '@material-ui/icons/ShowChart';
import LineChart from '../../components/LineChart';
import PieChart from '../../components/PieChart';

const useStyles = makeStyles((theme) => ({
    root: {
        padding: theme.spacing(3),
    },

   
  }));

function Home() {
    const classes = useStyles();
    const [user, setUser] = useState(JSON.parse(localStorage.getItem('profile')));
    console.log(user);

    useEffect(() => {
        setUser(JSON.parse(localStorage.getItem('profile')));
    },[])
    return (

        <div className={classes.root}>
            <Typography variant="h4" color="secondary">Hello, {user ? user.result.name : ''}</Typography>
            <Grid container >
                <Grid item xs={4}>
                    <Infobox
                    title = 'Revenue'
                    value = {user ? user.result.total_revenue :0}
                    icon = {<ShowChartIcon />}
                    />
                </Grid>
                <Grid item xs={4}>
                    <Infobox 
                    title = 'Profit'
                    value ={user ? user.result.total_profits : 0}
                    icon = {<ShowChartIcon />}
                    />
                </Grid>
                <Grid item xs={4}>
                    <Infobox 
                    title = 'Sales'
                    value = {user ? user.result.total_orders : 0}
                    icon = {<ShowChartIcon />}
                    />
                </Grid>
                <Grid item xs={8}>
                    <LineChart />
                </Grid>
                <Grid item xs={4}>
                    <PieChart />
                </Grid>
            </Grid>
            
        </div>
        
    )
}

export default Home
