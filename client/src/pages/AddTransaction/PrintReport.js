import { Grid, Paper , makeStyles, Typography} from '@material-ui/core'
import React, { useState } from 'react';
import DateMomentUtils from '@date-io/moment';
import {
  DatePicker,
  MuiPickersUtilsProvider,
} from '@material-ui/pickers';
import {  Form } from '../../components/useForm';
import Controls from '../../components/controls/Controls';

const useStyles = makeStyles(theme => ({
    pageContent: {
        padding: theme.spacing(3),
        margin: theme.spacing(5),
    },
    searchInput: {
        width: '75%'
    },
    newButton: {
        position: 'absolute',
        right: '10px'
    }
}))


function PrintReport() {
    const classes = useStyles();
    const [fromDate, setFromData] = useState(new Date());
    const [toDate, setToData] = useState(new Date());

    const handleSubmit = (e) => {
        e.preventDefault();
    };
    return (
       <>
       <Paper className= {classes.pageContent}>
        <MuiPickersUtilsProvider utils={DateMomentUtils}>
            <Typography variant="h3" color="initial">Print Report</Typography>
            <Form >
                <Grid container>
                    <Grid item xs={6}> 
                     <DatePicker disableFuture label='From Date' value={fromDate} onChange={setFromData} />  
                    </Grid>
                    <Grid item xs={6}>
                     <DatePicker disableFuture label='To Date' value={toDate} onChange={setToData} />  
                    </Grid>
                    <Grid item xs={12}>
                        console.log(fromDate);
                    <div>
                        <Controls.Button
                            type="submit"
                            text="Submit" />
                    </div>
                    </Grid>
                </Grid>
            </Form>
         </MuiPickersUtilsProvider>
         </Paper>
       </>
    )
}

export default PrintReport
