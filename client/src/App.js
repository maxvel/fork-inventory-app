import { makeStyles, CssBaseline, createMuiTheme, ThemeProvider } from '@material-ui/core';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import Sidebar from './components/Sidebar';
import Header from './components/Header';
import Home from './pages/home/Home';
import AddTransaction from './pages/AddTransaction/AddTransaction';
import AddItems from './pages/AddItem/AddItems';
import AddCategory from './pages/AddCategory/AddCategory';
import React, { useEffect } from 'react';
import {useDispatch} from 'react-redux';
import {getProducts} from './actions/products';
import Auth from './components/Auth';
import Profile from './pages/Profile/Profile';
import Setting from './pages/Setting/Setting';
import PrintReport from './pages/AddTransaction/PrintReport';


const theme = createMuiTheme({
  palette: {
    primary: {
      main: "rgb(222, 40, 40)",
      light: '#f8324526'
    },
    secondary: {
      main: "#ffffff",
      light: '#f8324526'
    },
    background: {
      default: "rgb(36, 36, 36);"
    },
    initial: {
      main:'#FFFFFF'
    }
  },
  overrides:{
    MuiAppBar:{
      root:{
        transform:'translateZ(0)'
      }
    }
  },
  props:{
    MuiIconButton:{
      disableRipple:true
    }
  },
  typography:{
    fontFamily:"space grotesk", 
  },
 
})

const useStyles = makeStyles({
  appMain: {
    paddingLeft: '80px',
    width: '100%',
  }
})


function App() {

  const classes = useStyles();
  const dispatch =useDispatch();

  useEffect(()=>{
    dispatch(getProducts());
  },[dispatch]);

  

  return (
    <Router>
      <ThemeProvider theme={theme}>
        <Sidebar />

        <div className={classes.appMain}>
        <Header />

          <Switch>
            <Route path='/' exact component={Home} />
            <Route path='/AddItems' exact component={AddItems} />
            <Route path='/AddTransaction' exact component={AddTransaction} />
            <Route path='/AddTransaction/printReport' exact component={PrintReport} />
            <Route path='/AddCategory' exact component={AddCategory} />
            <Route path='/Auth' exact component={Auth} />
            <Route path='/Profile' exact component={Profile} />
            <Route path='/Setting' exact component={Setting}/>
          </Switch>
        </div>
      <CssBaseline />
      </ThemeProvider>
    </Router>
  );
}

export default App;
